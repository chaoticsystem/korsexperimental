package com.korsengineering.modules.korsExperimental;

import javax.baja.sys.*;

public class BChangeTimer extends BComponent {
    @SuppressWarnings("WeakerAccess")
    public static final Property averageRuntime = newProperty(Flags.SUMMARY | Flags.READONLY, 0, null);
    @SuppressWarnings({"unused", "WeakerAccess"})
    public float getAverageRuntime() { return getFloat(averageRuntime); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setAverageRuntime(float v) { setFloat(averageRuntime, v, null); }

    @SuppressWarnings("WeakerAccess")
    public static final Property totalRuntime = newProperty(Flags.SUMMARY | Flags.READONLY, 0, null);
    @SuppressWarnings({"unused", "WeakerAccess"})
    public float getTotalRuntime() { return getFloat(totalRuntime); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setTotalRuntime(float v) { setFloat(totalRuntime, v, null); }

    @SuppressWarnings("WeakerAccess")
    public static final Action runTest = newAction(Flags.SUMMARY, null);
    @SuppressWarnings("unused")
    public void runTest(){ invoke(runTest, null, null); }
    @SuppressWarnings("unused")
    public void doRunTest() {
        int totalRuns = getIterations();
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < totalRuns; i++) {
            setTriggerValue(!getTriggerValue());
        }
        float totalTime = System.currentTimeMillis() - startTime;
        setTotalRuntime(totalTime);
        float averageTime = totalTime / getIterations();
        setAverageRuntime(averageTime);
    }

    @SuppressWarnings("WeakerAccess")
    public static final Property iterations = newProperty(Flags.SUMMARY, 1000, null);
    @SuppressWarnings({"unused", "WeakerAccess"})
    public int getIterations() { return getInt(iterations); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setIterations(int v) { setInt(iterations, v, null); }

    @SuppressWarnings("WeakerAccess")
    public static final Property triggerValue = newProperty(Flags.SUMMARY, false, null);
    @SuppressWarnings({"unused", "WeakerAccess"})
    public boolean getTriggerValue() { return getBoolean(triggerValue); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setTriggerValue(boolean v) { setBoolean(triggerValue, v, null); }

    @Override
    public Type getType() { return TYPE; }
    @SuppressWarnings("WeakerAccess")
    public static final Type TYPE = Sys.loadType(BChangeTimer.class);
}

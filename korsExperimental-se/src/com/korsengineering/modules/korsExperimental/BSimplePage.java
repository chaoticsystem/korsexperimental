package com.korsengineering.modules.korsExperimental;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import javax.baja.file.BIFile;
import javax.baja.io.HtmlWriter;
import javax.baja.naming.BOrd;
import javax.baja.sys.*;
import javax.baja.velocity.BVelocityView;
import javax.baja.web.BWebServlet;
import javax.baja.web.WebOp;

public class BSimplePage extends BWebServlet {
    @SuppressWarnings("WeakerAccess")
    public static final Property servletName = newProperty(Flags.SUMMARY, "experimental", null);
    @SuppressWarnings({"unused", "WeakerAccess"})
    public String getServletName() { return getString(servletName); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setServletName(String v) { setString(servletName, v, null); }

    @Override
    public Type getType() {
        return TYPE;
    }

    public static final Type TYPE = Sys.loadType(BSimplePage.class);

    @Override
    public void doGet(WebOp op) throws Exception {
        String viewPath = op.getRequest().getParameter("view"); //"file:^mach2/pages/roman-test.txt"
        String modelPath = op.getRequest().getParameter("model");
        BComponent model = (BComponent) BOrd.make(modelPath).get();
        BIFile file = (BIFile) BOrd.make(viewPath).get();
        String template = new String(file.read());


        VelocityEngine velocity = BVelocityView.getVelocityEngineInstance();

        //BVelocityView x = new BVelocityView();

        VelocityContext context = new VelocityContext();

        context.put("model", model);
        HtmlWriter w = op.getHtmlWriter();
        velocity.evaluate(context, w, "velocity", template);
        w.flush();
    }
}
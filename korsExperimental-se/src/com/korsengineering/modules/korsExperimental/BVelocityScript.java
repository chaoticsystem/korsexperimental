package com.korsengineering.modules.korsExperimental;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import javax.baja.sys.*;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BVelocityScript extends BComponent {

    VelocityContext velocityContext;

    public  BVelocityScript(){
        velocityContext = new VelocityContext();
    }

    @Override
    public void changed(Property property, Context context) {
        if (!Sys.atSteadyState() || !isRunning()) {
            return;
        }

        if (property == script) {
            runScript();
        }
    }

    private void runScript() {
        try {
            setError("");
            String template = getScript();
            StringWriter writer = new StringWriter();
            Velocity.evaluate(velocityContext, writer, "velocity", template);
            String rendered = writer.getBuffer().toString();
            setOutput(rendered);
        } catch (Exception e) {
            log.log(Level.WARNING, e.getMessage());
            setError(e.toString());
        }
    }

    @SuppressWarnings("WeakerAccess")
    public static final Action execute = newAction(Flags.SUMMARY, null);
    @SuppressWarnings("unused")
    public void execute(){ invoke(execute, null, null); }
    @SuppressWarnings("unused")
    public void doExecute() {
        runScript();
    }

    @SuppressWarnings("WeakerAccess")
    public static final Property script = newProperty(Flags.SUMMARY, "", null);
    @SuppressWarnings({"unused", "WeakerAccess"})
    public String getScript() { return getString(script); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setScript(String v) { setString(script, v, null); }

    @SuppressWarnings("WeakerAccess")
    public static final Property output = newProperty(Flags.SUMMARY | Flags.READONLY | Flags.TRANSIENT | Flags.DEFAULT_ON_CLONE, "", BFacets.make(BFacets.MULTI_LINE, BBoolean.TRUE, BFacets.FIELD_WIDTH, BInteger.make(80)));
    @SuppressWarnings({"unused", "WeakerAccess"})
    public String getOutput() { return getString(output); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setOutput(String v) { setString(output, v, null); }

    @SuppressWarnings("WeakerAccess")
    public static final Property error = newProperty(Flags.SUMMARY | Flags.READONLY | Flags.TRANSIENT | Flags.DEFAULT_ON_CLONE, "", BFacets.make(BFacets.MULTI_LINE, BBoolean.TRUE, BFacets.FIELD_WIDTH, BInteger.make(80)));
    @SuppressWarnings({"unused", "WeakerAccess"})
    public String getError() { return getString(error); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setError(String v) { setString(error, v, null); }

    @Override
    public Type getType() {
        return TYPE;
    }

    @SuppressWarnings("WeakerAccess")
    public static final Type TYPE = Sys.loadType(BVelocityScript.class);

    private static final Logger log = Logger.getLogger(TYPE.getModule().getModuleName() + "." + TYPE.getTypeName());
}

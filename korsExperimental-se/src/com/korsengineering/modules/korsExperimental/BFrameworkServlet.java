package com.korsengineering.modules.korsExperimental;

import com.tridium.session.NiagaraSuperSession;
import com.tridium.session.SessionManager;

import javax.baja.sys.Sys;
import javax.baja.sys.Type;
import javax.baja.web.BWebServlet;
import javax.baja.web.WebOp;

public class BFrameworkServlet extends BWebServlet {

    private String getCsrfToken() {
        NiagaraSuperSession session = SessionManager.getCurrentNiagaraSuperSession();
        return session != null ? session.getCsrfToken() : null;
    }

    @Override
    public void doGet(WebOp op) throws Exception {
        op.getResponse().setStatus(200);
        op.setContentType("text/html");
        op.getResponse().setHeader("Cache-Control", "no-cache, no-store");

        new Framework(op).startRendering();
    }

    @Override
    public Type getType() {
        return TYPE;
    }
    public static final Type TYPE = Sys.loadType(BFrameworkServlet.class);
}


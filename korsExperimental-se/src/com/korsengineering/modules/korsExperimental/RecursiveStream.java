package com.korsengineering.modules.korsExperimental;

import java.util.function.Function;
import java.util.stream.Stream;

public class RecursiveStream{
    public static <T> Stream<T> from(T head, Function<T, T> getNext) {
        if (head == null) {
            return Stream.empty();
        }
        T next = getNext.apply(head);
        if (head == next) {
            return Stream.empty();
        }
        return Stream.concat(Stream.of(head), from(next, getNext));
    }
}

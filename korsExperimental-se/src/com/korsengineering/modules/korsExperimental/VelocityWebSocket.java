package com.korsengineering.modules.korsExperimental;

import jdk.nashorn.internal.objects.annotations.Setter;
import org.apache.velocity.VelocityContext;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;
import org.json.JSONObject;

import javax.baja.sys.BComponent;
import javax.baja.sys.BComponentEvent;
import javax.baja.sys.BComponentEventMask;
import javax.baja.sys.Subscriber;
import javax.baja.tag.TagDictionary;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@WebSocket
public class VelocityWebSocket {
    public static final String SCRIPT_FOLDER = "file:^korsExperimental/scripts/ws-protocol/krotocol/";

    private Session session;
    private Map state = new HashMap();

    VelocityContext makeContext() {
        VelocityContext context = new VelocityContext();
        context.put("framework", Framework.class);
        context.put("webSocket", this);
        context.put("subscriber", this.subscriber);
        context.put("persistent", this.state);
        context.put("scriptFolder", SCRIPT_FOLDER);
        return context;
    }

    public Session getState() {
        return session;
    }

    public Session getSession() {
        return session;
    }

    private Subscriber subscriber = new Subscriber() {
        { setMask(BComponentEventMask.SELF_EVENTS); }

        @Override
        public void event(BComponentEvent event) {
            try {
                if (!session.isOpen()){
                    subscriber.unsubscribeAll();
                    return;
                }

                VelocityContext context = makeContext();

                context.put("event", event);
                String debugOutput =
                    Framework.runScript(SCRIPT_FOLDER + "subscriber_event.vm", context);
                log(debugOutput);
            } catch (Exception e){
                System.out.print(e.getMessage());
            }
        }
    };

    public Subscriber getSubscriber() {
        return subscriber;
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        this.session = session;

        String debugOutput =
        Framework.runScript(SCRIPT_FOLDER + "websocket_onConnect.vm", makeContext());
        System.out.println(debugOutput);
    }

    @OnWebSocketMessage
    public void onText(String message) {
        try {
            VelocityContext context = makeContext();
            context.put("message", message);
            String debugOutput =
                Framework.runScript(SCRIPT_FOLDER + "websocket_onText.vm", context);
            log(debugOutput);
        } catch (Exception e) {
            System.out.print(e.getMessage()); //TODO Handle errors through Velocity as well
        }
    }

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        VelocityContext context = makeContext();
        context.put("statusCode", statusCode);
        context.put("reason", reason);
        String debugOutput =
            Framework.runScript(SCRIPT_FOLDER + "websocket_onClose.vm", context);
        log(debugOutput);
    }

    @OnWebSocketError
    public void onError(Throwable cause) {
        VelocityContext context = makeContext();
        context.put("cause", cause);
        String debugOutput =
            Framework.runScript(SCRIPT_FOLDER + "websocket_onError.vm", context);
        log(debugOutput);
    }

    public void send(String str) throws IOException {
        if (session != null) {
            session.getRemote().sendStringByFuture(str);
        } else {
            throw new IOException("Web socket not initialized yet.");
        }
    }

    public void send(Map map) throws IOException {
        if (session != null) {
            String message = new JSONObject(map).toString();
            session.getRemote().sendStringByFuture(message);
        } else {
            throw new IOException("Web socket not initialized yet.");
        }
    }

    void log(String message) {
        if (message == null || message.matches("^\\s*$")) {
            return;
        }
        System.out.print(message.trim().replaceAll("\n\\s\n", "\n") + "\n\n--\n\n");
    }
}
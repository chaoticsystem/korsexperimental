package com.korsengineering.modules.korsExperimental;

import org.eclipse.jetty.websocket.servlet.*;
import javax.servlet.annotation.WebServlet;

@WebServlet
public class KorsWebSocketServlet extends WebSocketServlet {
    @Override
    public void configure(WebSocketServletFactory factory) {
        factory.register(VelocityWebSocket.class);
    }
}


package com.korsengineering.modules.korsExperimental.resources;

import javax.baja.status.BStatus;
import javax.baja.status.BStatusString;
import javax.baja.sys.*;
import java.util.UUID;

public class BStringResource extends BComponent {

    @SuppressWarnings("WeakerAccess")
    public static final Property id = newProperty(Flags.SUMMARY, UUID.randomUUID().toString(), null);
    @SuppressWarnings({"unused"})
    public String getId() { return getString(id); }
    @SuppressWarnings({"unused"})
    public void setId(String v) { setString(id, v, null); }

    @SuppressWarnings("WeakerAccess")
    public static final Property value = newProperty(Flags.SUMMARY, new BStatusString("", BStatus.nullStatus), null);
    @SuppressWarnings({"unused"})
    public BStatusString getValue() { return (BStatusString) get(value); }
    @SuppressWarnings({"unused"})
    public void setValue(BStatusString v) { set(value, v, null); }
    
    @Override
    public void changed(Property property, Context context) {
        if (!Sys.atSteadyState() || !isRunning()) {
            return;
        }
    }

    @Override
    public Type getType() {
        return TYPE;
    }

    @SuppressWarnings("WeakerAccess")
    public static final Type TYPE = Sys.loadType(BStringResource.class);
}
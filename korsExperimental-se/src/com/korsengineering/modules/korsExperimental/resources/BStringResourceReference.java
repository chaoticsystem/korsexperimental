package com.korsengineering.modules.korsExperimental.resources;

import com.korsengineering.modules.korsExperimental.RecursiveStream;
import com.tridium.tagdictionary.condition.BOr;
import org.jetbrains.annotations.NotNull;

import javax.baja.collection.BITable;
import javax.baja.collection.TableCursor;
import javax.baja.control.BPointExtension;
import javax.baja.naming.BOrd;
import javax.baja.naming.SlotPath;
import javax.baja.status.BStatus;
import javax.baja.status.BStatusString;
import javax.baja.sys.*;
import javax.baja.tag.Id;
import javax.baja.tag.Tag;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class BStringResourceReference extends BComponent { BPointExtension
    @SuppressWarnings("WeakerAccess")
    public static final Property scope = newProperty(Flags.SUMMARY, "", null);
    @SuppressWarnings({"unused", "WeakerAccess"})
    public String getScope() { return getString(scope); }
    @SuppressWarnings({"unused"})
    public void setScope(String v) { setString(scope, v, null); }

    @SuppressWarnings("WeakerAccess")
    public static final Property resourceId = newProperty(Flags.SUMMARY, "", null);
    @SuppressWarnings({"unused", "WeakerAccess"})
    public String getResourceId() { return getString(resourceId); }
    @SuppressWarnings({"unused"})
    public void setResourceId(String v) { setString(resourceId, v, null); }

    @SuppressWarnings("WeakerAccess")
    public static final Property value = newProperty(Flags.SUMMARY, new BStatusString("", BStatus.nullStatus), null);
    @SuppressWarnings({"unused", "WeakerAccess"})
    public BStatusString getValue() { return (BStatusString) get(value); }
    @SuppressWarnings({"unused"})
    public void setValue(BStatusString v) { set(value, v, null); }

    @SuppressWarnings("WeakerAccess")
    public static final Property scopeRootOrd = newProperty(Flags.READONLY | Flags.TRANSIENT, BOrd.make("station:|slot:/"), BFacets.make(BFacets.TARGET_TYPE, "baja:Component"));
    @SuppressWarnings({"unused", "WeakerAccess"})
    public BOrd getScopeRootOrd() { return (BOrd) get(scopeRootOrd); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setScopeRootOrd(BOrd v) { set(scopeRootOrd, v, null); }

    @SuppressWarnings("WeakerAccess")
    public static final Property resourceOrd = newProperty(Flags.TRANSIENT | Flags.READONLY, BOrd.NULL, BFacets.make(BFacets.TARGET_TYPE, "baja:Component"));
    @SuppressWarnings({"unused"})
    public BOrd getResourceOrd() { return (BOrd) get(resourceOrd); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setResourceOrd(BOrd v) { set(resourceOrd, v, null); }

    private static Id SCOPE_TAG = Id.newId("kors", "scope");

    @Override
    public void started(){
        attemptToLink();
    }

    @Override
    public void changed(Property property, Context context) {
        if (property == scope || property == resourceId) {
            attemptToLink();
        }
    }

    private void attemptToLink() {
        BOrd newRootOrd = recalculateScopeRoot(getScope());
        setScopeRootOrd(newRootOrd);

        onResourceIdChange(getResourceId());
    }

    private void onResourceIdChange(String resourceIdValue) {
        BOrd scopeRootOrd = getScopeRootOrd();
        if (scopeRootOrd.equals(BOrd.NULL)) {
            getValue().setStatus(BStatus.NULL);
            return;
        }

        BOrd newResourceOrd = BOrd.make(scopeRootOrd, "bql:select where type=korsExperimental:StringResource and id='" + SlotPath.escape(resourceIdValue) + "'");
        List<BStringResource> results = getMatchingResources(newResourceOrd);

        switch (results.size()) {
            case 0: //no matching resources
                setResourceOrd(newResourceOrd);
                getValue().setStatus(BStatus.DOWN);
            break;

            case 1: //normal case
                BStringResource resource = results.get(0);
                setResourceOrd(resource.getSlotPathOrd());
                BLink link = new BLink(resource.getHandleOrd(),"value","value",true);
                this.add(null, link);
                getValue().setStatus(BStatus.DEFAULT);
            break;

            default: //several matching resources
                setResourceOrd(newResourceOrd);
                getValue().setStatus(BStatus.FAULT);
            break;
        }
    }

    private static final BOrd STATION_ROOT = BOrd.make("station:|slot:/");

    @NotNull
    private List<BStringResource> getMatchingResources(BOrd ord) {
        BObject station = STATION_ROOT.get();
        return ((BITable<BStringResource>) ord.get(station))
            .cursor()
            .stream()
            .collect(Collectors.toList());
    }

    private BOrd recalculateScopeRoot(String scope) {
        if (scope.isEmpty()){
            return STATION_ROOT;
        }

        return RecursiveStream.from(this, BComplex::getParent) //all parents + this
            .map(BComplex::asComponent)
            .filter(c -> c.tags().get(SCOPE_TAG).isPresent())
            .filter(c -> c.tags().get(SCOPE_TAG).get().toString().equals(scope))
            .findFirst() //closest upwards
            .map(BComponent::getSlotPathOrd)
            .orElse(BOrd.NULL);
    }

    @Override
    public Type getType() {
        return TYPE;
    }
    @SuppressWarnings("WeakerAccess")
    public static final Type TYPE = Sys.loadType(BStringResourceReference.class);
}
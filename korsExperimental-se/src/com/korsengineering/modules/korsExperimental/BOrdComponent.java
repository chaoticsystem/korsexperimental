package com.korsengineering.modules.korsExperimental;

import javax.baja.naming.BOrd;
import javax.baja.sys.*;

public class BOrdComponent extends BComponent {
    @SuppressWarnings("WeakerAccess")
    public static final Property value = newProperty(Flags.SUMMARY, BOrd.DEFAULT, BFacets.make(BFacets.TARGET_TYPE, "baja:IFile"));
    @SuppressWarnings({"unused", "WeakerAccess"})
    public BOrd getValue() { return (BOrd) get(value); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setValue(BOrd v) { set(value, v, null); }

    @Override
    public Type getType() {
        return TYPE;
    }

    @SuppressWarnings("WeakerAccess")
    public static final Type TYPE = Sys.loadType(BOrdComponent.class);
}
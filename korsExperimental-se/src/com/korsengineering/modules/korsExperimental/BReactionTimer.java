package com.korsengineering.modules.korsExperimental;

import javax.baja.sys.*;

public class BReactionTimer extends BComponent {
    @SuppressWarnings("WeakerAccess")
    public static final Property averageRuntime = newProperty(Flags.SUMMARY | Flags.READONLY, 0, null);
    @SuppressWarnings({"unused", "WeakerAccess"})
    public float getAverageRuntime() { return getFloat(averageRuntime); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setAverageRuntime(float v) { setFloat(averageRuntime, v, null); }

    @SuppressWarnings("WeakerAccess")
    public static final Property totalRuntime = newProperty(Flags.SUMMARY | Flags.READONLY, 0, null);
    @SuppressWarnings({"unused", "WeakerAccess"})
    public float getTotalRuntime() { return getFloat(totalRuntime); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setTotalRuntime(float v) { setFloat(totalRuntime, v, null); }

    @SuppressWarnings("WeakerAccess")
    public static final Action react = newAction(Flags.SUMMARY, null);
    @SuppressWarnings("unused")
    public void react(){ invoke(react, null, null); }
    @SuppressWarnings("unused")
    public void doReact() {
        if (testRunning) {
            if (counter++ < cachedIterations) {
                setTriggerValue(getTriggerValue() + 1);
            } else {
                doStopTest();
            }
        }
    }

    @SuppressWarnings("WeakerAccess")
    public static final Action stopTest = newAction(Flags.SUMMARY, null);
    @SuppressWarnings("unused")
    public void stopTest(){ invoke(stopTest, null, null); }
    @SuppressWarnings("unused")
    public void doStopTest() {
        testRunning = false;
        long totalTime = System.currentTimeMillis() - startTime;
        setTotalRuntime(totalTime);
        setAverageRuntime((float)totalTime / cachedIterations);
        setTriggerValue(0);
        counter = 0;
    }

    boolean testRunning = false;
    int cachedIterations = 0;
    int counter = 0;
    long startTime = 0;

    @SuppressWarnings("WeakerAccess")
    public static final Action startTest = newAction(Flags.SUMMARY, null);
    @SuppressWarnings("unused")
    public void startTest(){ invoke(startTest, null, null); }
    @SuppressWarnings("unused")
    public void doStartTest() {
        cachedIterations = getIterations();
        setTriggerValue(0);
        testRunning = true;
        startTime = System.currentTimeMillis();
        setTriggerValue(1);
    }


    @SuppressWarnings("WeakerAccess")
    public static final Property triggerValue = newProperty(Flags.SUMMARY, 0, null);
    @SuppressWarnings({"unused", "WeakerAccess"})
    public int getTriggerValue() { return getInt(triggerValue); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setTriggerValue(int v) { setInt(triggerValue, v, null); }

    @SuppressWarnings("WeakerAccess")
    public static final Property iterations = newProperty(Flags.SUMMARY, 1000, null);
    @SuppressWarnings({"unused", "WeakerAccess"})
    public int getIterations() { return getInt(iterations); }
    @SuppressWarnings({"unused", "WeakerAccess"})
    public void setIterations(int v) { setInt(iterations, v, null); }

    @Override
    public Type getType() { return TYPE; }
    @SuppressWarnings("WeakerAccess")
    public static final Type TYPE = Sys.loadType(BReactionTimer.class);
}

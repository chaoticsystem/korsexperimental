package com.korsengineering.modules.korsExperimental;

import com.tridium.session.NiagaraSuperSession;
import com.tridium.session.SessionManager;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.json.JSONObject;

import javax.baja.data.BIDataValue;
import javax.baja.naming.BOrd;
import javax.baja.naming.UnresolvedException;
import javax.baja.sys.BComponent;
import javax.baja.sys.BObject;
import javax.baja.tag.Id;
import javax.baja.velocity.BVelocityView;
import javax.baja.web.WebOp;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Framework {
    WebOp op;

    static String VIEW_FOLDER = "file:^korsExperimental/views/";

    public Framework(WebOp op) {
        this.op = op;
    }

    public void writeRaw(String text) throws IOException {
        op.getHtmlWriter().write(text);
    }

    public void startRendering() throws IOException {
        render(VIEW_FOLDER + "_root.vm", null);
    }

    public void render(Object model) throws IOException {
        render(VIEW_FOLDER + "_view_frame.vm", model);
    }

    public void render(String viewOrd, Object model) throws IOException {
        VelocityEngine engine = BVelocityView.getVelocityEngineInstance();
        Template template = engine.getTemplate(viewOrd);
        VelocityContext context = BVelocityView.makeDefaultVelocityContext(op, null);
        context.put("viewFolder", VIEW_FOLDER);
        context.put("framework", this);
        context.put("model", model);

        PrintWriter out = op.getWriter();
        template.merge(context, out);
        out.flush();
    }

    //Static methods for using in velocity scripts that aren't web views:

    public static String getCsrfToken() {
        NiagaraSuperSession session = SessionManager.getCurrentNiagaraSuperSession();
        return session != null ? session.getCsrfToken() : null;
    }

    public static Stream<Class> getInheritanceChain(Class current){
        if (current == Object.class){
            return Stream.of(Object.class);
        } else {
            return Stream.concat(Stream.of(current), getInheritanceChain(current.getSuperclass()));
        }
    }

    public static String toViewOrd(Class c) {
        return "file:^korsExperimental/views/" + c.getCanonicalName() + ".vm";
    }

    public static String getViewByClass(Class current) throws FileNotFoundException {
        return getInheritanceChain(current)
                .map(Framework::toViewOrd)
                .filter(Framework::ordResolves)
                .findFirst()
                .orElseThrow(() -> new FileNotFoundException("Could not find any view for type " + current));
    }

    public static Optional<String> getViewByTag(Object model){
        if (!(model instanceof BComponent)) {
            return Optional.empty();
        }

        BComponent component = (BComponent) model;
        Optional<BIDataValue> viewFileTag = component.tags().get(Id.newId("kors:viewFile"));
        return viewFileTag
            .filter(x -> !x.equals(BOrd.NULL))
            .map(x -> ((BOrd) x).encodeToString());
    }

    public static String runScript(String scriptOrd, Map context){
        return runScript(scriptOrd, new VelocityContext(context));
    }

    public static String runScript(String scriptOrd, VelocityContext context){
        VelocityEngine engine = BVelocityView.getVelocityEngineInstance();
        Template template = engine.getTemplate(scriptOrd);
        StringWriter out = new StringWriter();
        template.merge(context, out);
        out.flush();
        return out.toString();
    }

    public static boolean ordResolves(String ord){
        try{
            BOrd.make(ord).get();
            return true;
        } catch (UnresolvedException e) {
            return false;
        }
    }

    public static BObject getBObject(String ord){
        try{
            return BOrd.make(ord).get();
        } catch (UnresolvedException e) {
            return null;
        }
    }

    public static Object parseJson(String json) {
        return new JSONObject(json);
    }


    public static Object makeJsonObject(Map<?, ?> map) {
        return new JSONObject(map);
    }

    public static Object getClass(String name) throws ClassNotFoundException {
        return Class.forName(name);
    }

    public static void throwException(String message) throws Exception {
        throw new Exception(message);
    }

    public static Object getIntanceOf(String name) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        return Class.forName(name).newInstance();
    }

    public static Object make(String className, Object[]... params) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class[] parameterClasses = Arrays.stream(params)
            .map(Object[]::getClass)
            .toArray(Class[]::new);
        return Class.forName(className).getConstructor(parameterClasses).newInstance((Object[]) params);
    }
}
package com.korsengineering.modules.korsExperimental;

import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.event.EventCartridge;
import org.apache.velocity.app.event.InvalidReferenceEventHandler;
import org.apache.velocity.app.event.ReferenceInsertionEventHandler;
import org.apache.velocity.runtime.RuntimeInstance;
import org.apache.velocity.runtime.parser.ParseException;
import org.apache.velocity.runtime.parser.node.ASTReference;
import org.apache.velocity.runtime.parser.node.ParserVisitor;
import org.apache.velocity.runtime.parser.node.SimpleNode;
import org.apache.velocity.runtime.visitor.BaseVisitor;

import java.io.StringWriter;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TemplateAnalyzer{

    public static Set<String> getReferences(String template){
        HashSet<String> names = new HashSet<>();

        VelocityContext velocityContext = new VelocityContext();
        EventCartridge ec = new EventCartridge();
        ReferenceInsertionEventHandler record = (reference, value) -> names.add(reference);
        ec.addEventHandler(record);
        ec.attachToContext(velocityContext);

        Velocity.evaluate(velocityContext, new StringWriter(), "velocity", template);
        return names;
    }

    private static Pattern namePattern = Pattern.compile("\\$!?\\{?([a-zA-Z][\\w\\-]*)");

    public static Set<String> getVariableNames(String template)
    {
        Set<String> references = getReferences(template);
        return references.stream()
            .map(r -> namePattern.matcher(r))
            .filter(Matcher::find)
            .map(m -> m.group(1))
            .collect(Collectors.toSet());
    }
}

package com.korsengineering.modules.korsExperimental;

import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.testng.Assert.*;

public class FrameworkTest {

    @Test
    public void testGetInheritanceChain() {
        List<Class> results = Framework.getInheritanceChain(String.class)
            .collect(Collectors.toList());
        assertEquals(results, Arrays.asList(String.class, Object.class));
    }
}
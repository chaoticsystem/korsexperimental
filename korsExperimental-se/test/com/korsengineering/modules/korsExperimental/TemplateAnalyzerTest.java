package com.korsengineering.modules.korsExperimental;

import org.testng.annotations.Test;

import static org.testng.Assert.*;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Stream;

public class TemplateAnalyzerTest {
    String template = "test $n-a_me1 ${n-a_me2} $!n-a_me3 $!{n-a_me4} ";

    @Test
    public void testGetReferences() {
        Set<String> result = TemplateAnalyzer.getReferences(template);

        Stream.of("$n-a_me1", "${n-a_me2}", "$!n-a_me3", "$!{n-a_me4}").forEach(expected ->
            assertTrue(result.contains(expected), "Does not contain " + expected)
        );
        assertEquals(result.size(), 4);
    }

    @Test
    public void testGetInputs() {
        Set<String> result = TemplateAnalyzer.getVariableNames(template);

        Stream.of("n-a_me1", "n-a_me2", "n-a_me3", "n-a_me4").forEach(expected ->
            assertTrue(result.contains(expected), "Does not contain " + expected)
        );
        assertEquals(result.size(), 4);
    }
}
package com.korsengineering.modules.korsExperimental;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.testng.Assert.*;

public class PerformanceTest {

    int units = 100_000;
//    String unit = "This is an simple string.zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
//
//    @BeforeMethod
//    public void setUp() {
//        System.gc();
//    }
//
//    @Test(enabled = false)
//    public void string_plus(){
//        String result = "";
//        for (int i = 0; i < units; i++) {
//            result += unit;
//        }
//        assertEquals(units * unit.length(), result.length());
//    }
//
//    @Test(enabled = false)
//    public void string_concatenation(){
//        String result = "";
//        for (int i = 0; i < units; i++) {
//            result = result.concat(unit);
//        }
//        assertEquals(units * unit.length(), result.length());
//    }
//
//    @Test
//    public void string_builder(){
//        StringBuilder result = new StringBuilder();
//        for (int i = 0; i < units; i++) {
//            result.append(unit);
//        }
//        assertEquals(units * unit.length(), result.toString().length());
//    }
//
//    @Test
//    public void string_functional_join(){
//        String[] strings = IntStream.range(0, units)
//            .mapToObj(x -> unit)
//            .toArray(String[]::new);
//        String result = String.join("", strings);
//        assertEquals(units * unit.length(), result.length());
//    }
//
//    @Test
//    public void string_functional_concat(){
//        String result = IntStream.range(0, units)
//            .mapToObj(x -> unit)
//            .collect(Collectors.joining());
//        assertEquals(units * unit.length(), result.length());
//    }
//
//    @Test
//    public void string_parallel_functional_concat(){
//        String result = IntStream.range(0, units)
//            .parallel()
//            .mapToObj(x -> unit)
//            .collect(Collectors.joining());
//        assertEquals(units * unit.length(), result.length());
//    }
//
//    @Test(enabled = false)
//    public void list_string(){
//        List<String> result = IntStream.range(0, 10000000)
//            .mapToObj(String::valueOf)
//            .collect(Collectors.toList());
//        assertEquals(10000000, result.size());
//    }
//
//    @Test(enabled = false)
//    public void list_lambda (){
//        List<Supplier<String>> result = IntStream.range(0, 10000000)
//            .<Supplier<String>>mapToObj(x -> (() -> ""+x))
//            .collect(Collectors.toList());
//        assertEquals(10000000, result.size());
//    }
}
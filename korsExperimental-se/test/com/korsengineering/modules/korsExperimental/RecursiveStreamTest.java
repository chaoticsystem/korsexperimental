package com.korsengineering.modules.korsExperimental;

import org.testng.annotations.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.testng.Assert.*;

public class RecursiveStreamTest {
    @Test
    public void testFrom() {
        Stream<Class> result = RecursiveStream.from(String.class, Class::getSuperclass);
        List<Class> list = result.collect(Collectors.toList());

        assertEquals(list.size(), 2);
        assertEquals(list.get(0), String.class);
        assertEquals(list.get(1), Object.class);
    }
}